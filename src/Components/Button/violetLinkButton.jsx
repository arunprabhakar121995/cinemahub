import React from "react";
import { Link } from "react-router-dom";


function VioletButton({ path, buttonName }){
    return(
        <>
        <Link className="text-xl text-white bg-violet-700 hover:bg-violet-500 px-10 py-3 rounded-lg" to={path}>{buttonName}</Link>
        </>
    )
}

export default VioletButton