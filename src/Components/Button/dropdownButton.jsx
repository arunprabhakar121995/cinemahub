import React from "react";
import { Link, } from "react-router-dom";
import { useSelector } from "react-redux";


function DropdownButton(props){
    const loggedIn = useSelector((state) => state.login.loggedIn);
    return(
        <>
        <div className="md:hidden dropdown inline-block">
                <button className="justify-center">
                    <span className="material-symbols-outlined text-5xl text-white hover:text-violet-300 ease-in-out duration-300">
                        menu
                    </span>
                </button>
                <div className="content drop-shadow-lg hidden absolute min-w-28 text-violet-950 bg-violet-400 right-6 rounded-md">
                        <Link className="block px-10 py-3 text-xl bg-violet-300 text-violet-950 hover:text-violet-300 hover:bg-violet-950 ease-in-out duration-300 rounded-t-md" to="/">Home</Link>
                        <Link className="block px-10 py-3 text-xl bg-violet-300 text-violet-950 hover:text-violet-300 hover:bg-violet-950 ease-in-out duration-300" to="/movies">Movies</Link>
                        <Link className="block px-10 py-3 text-xl bg-violet-300 text-violet-950 hover:text-violet-300 hover:bg-violet-950 ease-in-out duration-300" to="/actors">Actors</Link>
                        <Link className="block px-10 py-3 text-xl bg-violet-300 text-violet-950 hover:text-violet-300 hover:bg-violet-950 ease-in-out duration-300" to="/genres">Genres</Link>
                        <div className="border-t-2  border-white">
                            {
                                loggedIn?
                                    <Link className="block px-10 py-3 text-xl bg-violet-300 text-violet-950 hover:text-violet-300 hover:bg-violet-950 ease-in-out duration-300 rounded-b-md" to="/logout">Logout</Link>:
                                    <Link className="block px-10 py-3 text-xl bg-violet-300 text-violet-950 hover:text-violet-300 hover:bg-violet-950 ease-in-out duration-300 rounded-b-md" to="/login">Login</Link>
                            }
                        </div>
                        
                        
                    </div>
            </div>
        </>
    )
}

export default DropdownButton