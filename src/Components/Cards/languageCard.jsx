import React from "react";

function LanguageCard(props){
    const language = props.language
    return(
            <option value={language.title}>{language.title}</option>
    )
}

export default LanguageCard