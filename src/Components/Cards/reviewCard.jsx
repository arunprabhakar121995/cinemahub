import React from "react";

function ReviewCard(props){
    const review = props.reviews
    return(
        <>
        <article className="px-5 mx-14 p-2 border-4 border-violet-500 bg-violet-100">
            <div className="flex flex-row gap-2">
            <p className="text-xl font-bold m-3 my-auto px-4 py-2 bg-violet-600 text-white rounded-full">{review.user.name.charAt(0)}</p>  
            <p className="text-xl font-bold text-violet-600 p-3 py-auto">{review.user.name}</p>
            </div>
            <h2 className="font-bold text-lg mt-3 pt-2 border-t-2 border-t-white">{review.title}</h2>
            <p className="text-md pt-3">{review.description}</p>
        </article>
        </>
    )
}

export default ReviewCard

//charAt(0) to get first letter