import React from "react";
import { Link } from "react-router-dom";
function ActorCard(props){
    const actor = props.actor
    return(
        <>
        <article className="m-5">
                    <Link className="p-5" to ={`/actors/${actor._id}`}>
                    <img className="rounded-full object-cover w-full shadow-2xl hover:shadow-violet-600" src={actor.image} alt="" />
                        <div className="flex flex-col px-2 mt-3 shadow-2xl shadow-violet-200 hover:shadow-violet-600 h-auto sm:h-48">
                            <h3 className="text-4xl font-bold">{actor.name}</h3>
                            <span className="text-md pt-2">{actor.occupation}</span>
                        </div>
                    </Link>
                </article>
        </>
    )
}

export default ActorCard