import React from "react";
import { Link } from "react-router-dom";

function GenreCard(props){
    const genre = props.genre
    return(
        <>
        <article className="p-3 hover:border rounded-lg hover:shadow-inner shadow-2xl shadow-black-400 hover:shadow-violet-600">
                    <Link to ={`/genres/${genre._id}`}>
                        <img className="rounded-xl object-cover w-full" src={genre.thumbnail} alt="" />
                        <div className="flex flex-col px-2 mt-3">
                            <h3 className="text-lg font-bold">{genre.title}</h3>
                        </div>
                    </Link>
                </article>
        </>
    )
}

export default GenreCard