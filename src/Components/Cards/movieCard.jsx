import React from "react";
import { Link } from "react-router-dom";
function formatDuration(durationInMinutes) {  //Function For Duration
    const hours = Math.floor(durationInMinutes / 60);
    const minutes = durationInMinutes % 60;
    return `${hours}hrs ${minutes}min`;
}
function MovieCard(props){
    const movie = props.movie
    movie.formattedDuration = formatDuration(movie.duration);
    return(
        <>
        <article className="p-5 hover:border rounded-lg hover:shadow-inner shadow-2xl shadow-black-400 hover:shadow-violet-600">
                    <Link to ={`/movies/${movie._id}`}>
                        <img className="rounded-xl object-cover w-full" src={movie.thumbnail} alt="" />
                        <div className="flex flex-col px-2 mt-3">
                            <h3 className="text-lg font-bold">{movie.title}</h3>
                            <span>{`Duration ${movie.formattedDuration}`}</span>
                        </div>
                    </Link>
                </article>
        </>
    )
}

export default MovieCard