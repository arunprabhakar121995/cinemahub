import { useForm } from 'react-hook-form';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import { addOneReview } from '../../features/review/reviewSlice';
const DB_URL = import.meta.env.VITE_DB_URL

function ReviewForm(props) {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset, // Import the reset function
  } = useForm();

  const dispatch = useDispatch()
  
  const onSubmit = (data) => { 
    const payload = {
      ...data,
      movie: props.movieId
    }
    axios.post(`${DB_URL}/reviews`,payload, {withCredentials: true}) //To let axios know we use cookies
    .then(response => {
      dispatch(addOneReview(response.data));
      window.alert("Added Review")
      reset() //to reset fields
    })
    
    .catch(error => {
      window.alert("Unable to add Review. Please try again")
    })
  }

  return (
    <form className='m-auto flex flex-col align-center mx-5 content-center max-w-full' onSubmit={handleSubmit(onSubmit)}>

      <label htmlFor="title">Title:</label>
      <input type="title" id="title" placeholder="Type your Title" className="border border-2 border-violet-500 p-2 my-2" {...register("title", { required: true } )} />
      {errors.title && <span className="text-red-600">You should enter a Title to add a Review</span>}

      <label htmlFor="description">Review</label>
      <textarea type="text" id="description" rows={10} placeholder="Comment Your Review Here" className="w-full border border-2 border-violet-500 p-2 my-2 resize-none" {...register("description", { required: true, maxLength: 1000})} />
      {errors.description && <p>Please enter your Review.</p>}
      <input type="submit" value={"Add Review"} className="mt-10 bg-violet-600 hover:bg-violet-500 m-auto px-10 py-2 mb-5 rounded-xl text-xl hover:text-2xl text-white hover:font-bold transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-300" />
    </form>
  )
}

export default ReviewForm