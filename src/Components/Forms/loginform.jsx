import { useForm } from "react-hook-form"
import axios from "axios"
import { useNavigate } from "react-router-dom"
import { useDispatch } from "react-redux"
import { changeLoginStatus } from "../../features/login/loginSlice"
import { useState } from "react"
const DB_URL = import.meta.env.VITE_DB_URL

export default function LoginForm() {
  const [showPassword, setShowPassword] = useState(false)
  const dispatch = useDispatch()
  const {
    register, 
    handleSubmit, 
    watch,
    formState: { errors },
  } = useForm()
  const navigate = useNavigate()


  const onSubmit = (data) => { 
    axios.post(`${DB_URL}/auth/login`, data, {withCredentials: true}) //To let axios know we use cookies
    .then(response => {
      dispatch(changeLoginStatus(true))
      window.alert("Logged In")
      navigate("/")
  })
    
    .catch(error => {
      dispatch(changeLoginStatus(false))
      window.alert("Invalid Credentials")
    })
  }


  console.log(watch("example"))


  return (
    <form onSubmit={handleSubmit(onSubmit)} className="flex flex-col gap-5">

        <label htmlFor="name">E-Mail:</label>
        <input type="email" id="email" placeholder="Type Your E-Mail" className="border border-gray-900 p-2" {...register("email", { required: true, pattern: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/ })} />
        {errors.email && <span className="text-red-600">Invalid E-Mail</span>}

        <label htmlFor="password">Password:</label>
        <div className="relative">
          <input type={showPassword ? "text" : "password"} id="password" placeholder="Type Your Password" className="border border-gray-900 p-2 w-full" {...register("password", { required: true } )} />
          <span className="absolute right-2 top-2 cursor-pointer" onClick={() => setShowPassword(!showPassword)}
          >
            {showPassword ? "Hide" : "Show"}
          </span>
        </div>
        {errors.password && <span className="text-red-600">You should enter your password to login</span>}

      <input type="submit" value={"Login"} className="bg-violet-600 hover:bg-violet-500 m-auto px-10 py-2 mb-5 rounded-xl text-xl hover:text-2xl text-white hover:font-bold  transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-300" />
    </form>
  )
}

