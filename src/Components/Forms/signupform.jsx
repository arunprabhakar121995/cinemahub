import { useForm } from "react-hook-form"
import axios from "axios"
import { useNavigate } from "react-router-dom"
import { useState } from "react"
const DB_URL = import.meta.env.VITE_DB_URL


export default function SignUpForm() {
  const [showPassword, setShowPassword] = useState(false)
  const [showConfirmPassword, setShowConfirmPassword] = useState(false)

  const {
    register, 
    handleSubmit, 
    watch,
    formState: { errors },
  } = useForm()
  const navigate = useNavigate()


  const onSubmit = (data) => {
    // Remove confirmPassword from data object
const { confirmPassword, ...formData } = data;

    axios.post(`${DB_URL}/users`, formData)
            .then(response => {
                const username = formData.name
                window.alert(`Hello ${username}. Welcome to the CinemaHub Family!!!`)
                navigate("/login");
            })
            .catch(error => console.log(error))
            //To make a post request
  }


  console.log(watch("example"))


  return (
    <form onSubmit={handleSubmit(onSubmit)} className="flex flex-col gap-5">
        <label htmlFor="name">Name:</label>
        <input type="text" id="name" placeholder="Type Your Name" className="border border-gray-900 p-2" {...register("name", { required: true, maxLength: 25 })} />
        {errors.name && <span className="text-red-600">Please Check Your Name</span>}

        <label htmlFor="email">E-Mail:</label>
        <input type="email" id="email" placeholder="Type Your E-Mail" className="border border-gray-900 p-2" {...register("email", { required: true, pattern: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/ })} />
        {errors.email && <span className="text-red-600">Invalid E-Mail</span>}

        <label htmlFor="password">Password:</label>
        <div className="relative">
          <input type={showPassword ? "text" : "password"} id="password" placeholder="Type Your Password" className="border border-gray-900 p-2 w-full" {...register("password", { required: true, pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&.])[A-Za-z\d@$!%*?&.]{8,}$/ } )} />
          <span className="absolute right-2 top-2 cursor-pointer" onClick={() => setShowPassword(!showPassword)}
          >
            {showPassword ? "Hide" : "Show"}
          </span>
        </div>
        {errors.password && <span className="text-red-600">Your Password should contain atleast 8 characters, one uppercase, one lowercase, one digit, one special character</span>}

        <label htmlFor="confirmpassword">Confirm Password:</label>
        <div className="relative">
          <input type={showConfirmPassword ? "text" : "password"} id="confirmpassword" placeholder="Type Your Password" className="border border-gray-900 p-2 w-full" {...register("confirmpassword", { required: true, validate: value => value === watch('password') || "Passwords does not match" } )} />
          <span className="absolute right-2 top-2 cursor-pointer" onClick={() => setShowConfirmPassword(!showConfirmPassword)}
          >
            {showConfirmPassword ? "Hide" : "Show"}
          </span>
        </div>
        {errors.password && <span className="text-red-600">Your Password doesn't Match</span>}

      <input type="submit" className="bg-violet-600 hover:bg-violet-500 m-auto px-10 py-2 mb-5 rounded-xl text-xl hover:text-2xl text-white hover:font-bold transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-300" />
    </form>
  )
}