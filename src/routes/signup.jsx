import React from "react";
import { Link } from "react-router-dom";
import SignUpForm from "../Components/Forms/signupform";


function SignUp(props){
    return(
        <>
        <main className="container px-5 flex flex-col m-auto max-w-screen-md">
            <section>
                <h2 className="text-6xl my-10">SIGNUP</h2>
                <SignUpForm />
                <div className="flex flex-col items-center justify-between container m-auto">
                    <span>If you already have an account</span>
                    <Link className="bg-violet-600 hover:bg-violet-500 px-10 py-2 mb-12 rounded-xl text-xl hover:text-2xl text-white hover:font-bold" to="/login">Login</Link>
                </div>
            </section>
        </main>
        </>
    );
}

export default SignUp