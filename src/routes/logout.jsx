import axios from "axios";
import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux"
import { changeLoginStatus } from "../features/login/loginSlice"
const DB_URL = import.meta.env.VITE_DB_URL

function Logout(props){
    const dispatch = useDispatch()
    const navigate = useNavigate();
    useEffect(() => {
        axios.get(`${DB_URL}/auth/logout`, {withCredentials: true})
        .then(res => {
            dispatch(changeLoginStatus(false))
            navigate("/login");
        })
        .catch(error => {
            dispatch(changeLoginStatus(true))
            console.log(error)
        })
    })
    return(
        <>
            <main className="container m-auto">
                <div className="flex flex-col items-center justify-center h-full mt-52">
                <h1 className="text-4xl md:text-5xl lg:text-6xl font-bold">Logging Out...........</h1>
                </div>
            </main>
        </>
    );
}

export default Logout