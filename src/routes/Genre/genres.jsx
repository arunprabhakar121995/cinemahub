import React from "react";
import { Link,useLoaderData } from "react-router-dom";
import GenreCard from "../../Components/Cards/genreCard";
import axios from "axios";
const DB_URL = import.meta.env.VITE_DB_URL

export async function loader() {
    const response = await axios.get(`${DB_URL}/genres`);
    const genres = response.data  //This line is coz axios give data inside data field
    return { genres };
  }

function Genres(props){

    const { genres } = useLoaderData();

    return(
        <>
        <main className="my-10">
        <section className="p-12">
            <div className="container mx-auto">
                <h2 className="text-5xl p-5 font-bold font-sanserif mb-5">Genres</h2>
                <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-5">
                    {
                        genres.map(genre => {
                            return(
                                <GenreCard key={genres._id} genre={genre} />
                            )
                        })
                    }                        
                </div>
            </div>
        </section>
        </main>
        </>
    )
}

export default Genres