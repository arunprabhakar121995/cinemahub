import React from "react";
import { useLoaderData } from "react-router-dom";
import axios from "axios";
import MovieCard from "../../Components/Cards/movieCard";
const DB_URL = import.meta.env.VITE_DB_URL


  export async function loader({params}) {
    const genreresponse = await axios.get(`${DB_URL}/genres/${params.genreId}`)
    const genre = genreresponse.data  //This line is coz axios give data inside data field

    // Fetch movies associated with the genre
    const moviesResponse = await axios.get(`${DB_URL}/movies`, {
        params: { genreId: params.genreId },
    });

    const movies = moviesResponse.data;

    return { genre, movies }
  }
function Genre(props) {
    const { genre, movies } = useLoaderData();

    return (
        <>
            <main className="my-5 flex flex-col container m-auto">
                <section className="p-5 flex flex-row content-between">
                    <div className="gap-2 flex items-center justify-between">
                        <img className="flex items-center max-h-24 px-2 rounded-full object-cover" src={genre.thumbnail} alt="" />
                        <h1 className="text-5xl px-2 font-bold font-sanserif">{genre.title}</h1>
                    </div>
                    
                </section>
                <section className="flex m-auto">
                    <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-5">
                    {
                        movies.map(movie => {
                            return(
                                <MovieCard key={movies._id} movie={movie} />
                            )
                        })
                    }                        
                </div>
                </section>
            </main>
        </>
    )
}

export default Genre