import React from "react";
import LoginForm from "../Components/Forms/loginform";
import { Link } from "react-router-dom";


function Login(props){
    return(
        <>
        <main className="container px-5 flex flex-col m-auto max-w-screen-md">
            <section>
                <h2 className="text-6xl my-10">LOGIN</h2>
                <LoginForm />
                <div className="flex flex-col items-center justify-between container m-auto">
                    <span>If you don't have an Account</span>
                    <Link className="bg-violet-600 hover:bg-violet-500 px-10 py-2 mb-12 rounded-xl text-xl hover:text-2xl text-white hover:font-bold transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-300" to="/signup">SignUp</Link>
                </div>
            </section>
        </main>
        </>
    )
}

export default Login