import React, { useEffect, useState } from "react";
import { Link, useLoaderData } from "react-router-dom";
import axios from "axios";
import { useDispatch, useSelector } from 'react-redux'
import ReviewCard from "../../Components/Cards/reviewCard";
import ReviewForm from "../../Components/Forms/reviewform";
import { addReviews } from "../../features/review/reviewSlice";
import VioletLinkButton from "../../Components/Button/violetLinkButton";
const DB_URL = import.meta.env.VITE_DB_URL

function formatDuration(durationInMinutes) {  //Function For Duration
    const hours = Math.floor(durationInMinutes / 60);
    const minutes = durationInMinutes % 60;
    return `${hours}hrs ${minutes}min`;
}
  export async function loader({params}) {
    const response = await axios.get(`${DB_URL}/movies/${params.movieId}`);
    const movie = response.data  //This line is coz axios give data inside data field

        // Convert ISO string date to text format
        const releaseDate = new Date(movie.releasedate).toLocaleDateString('en-IN', {
            timeZone: 'Asia/Kolkata', // Indian time zone
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        });

        movie.formattedReleaseDate = releaseDate;

        movie.formattedDuration = formatDuration(movie.duration); //Adding the convertion function here
    return { movie };
  }
function Movie(props){
    const loggedIn = useSelector(state => state.login.loggedIn)
    const { movie } = useLoaderData()
    const [user, setUser] = useState(null);
    const reviews = useSelector(state => state.review.reviews)
    const dispatch = useDispatch()

    useEffect(() => {
        axios.get(`${DB_URL}/reviews?movie=${movie._id}`)
            .then (async response => {
                const reviews = response.data;
                const userIds = [...new Set(reviews.map(review => review.user))];

                const userRequests = userIds.map(userId => axios.get(`${DB_URL}/users/${userId}`));
                const userResponses = await Promise.all(userRequests);

                const usersData = userResponses.reduce((acc, response) => {
                    acc[response.data._id] = response.data;
                    return acc;
                }, {});
                dispatch(addReviews(reviews))
            })
            .catch(error => console.log(error));
    }, [dispatch, movie._id]);
    return(
        
        <>
        <main className="container mx-auto">
            <section className="flex flex-col lg:grid grid-cols-2 my-16 px-5 gap-20 justify-center items-center">
                <div className="w-full">
                    <img className="flex items-center w-full rounded-lg" src={movie.thumbnail} alt="" />
                </div>
                <div className="flex flex-col justify-center bg-violet-200 p-5 rounded-xl flex-wrap w-full">
                    <h1 className="text-3xl sm:text-4xl text-violet-800 font-bold py-10 pl-5">{movie.title}</h1>
                    {movie.languages && 
                        <div className="border-t-2 pb-5 pt-3 border-white flex flex-wrap">
                            {movie.languages.map(language => (
                                <span key={language} className="py-2 px-3 mt-2 mr-2 text-lg font-semibold rounded-full bg-violet-500 text-white">{language}</span>
                            ))}
                        </div>}
                    <div className="text-xl flex flex-col sm:flex-row lg:flex-col xl:flex-row sm:justify-between border-t-2  border-white">
                        <div className="flex flex-row">                            
                            <span className="pr-2 py-3 lg:py-2 xl:py-3">Duration:</span>
                            <span className="py-3 lg:py-2 xl:py-3">{movie.formattedDuration}</span>
                        </div>
                        <div className="flex flex-row border-t-2 sm-border-none lg:border-t-2 xl:border-t-0 border-white">
                            <span className="pr-2 py-3 lg:py-2 xl:py-3">Released On: </span>
                            <span className="py-3 lg:py-2 xl:py-3">{movie.formattedReleaseDate}</span>
                        </div>
                    </div>
                    {movie.genres &&
                            <div className="border-t-2 pt-5 gap-y-10 pb-5 border-white flex flex-wrap">
                                {movie.genres.map(genre => (
                                    <Link key={genre.id} to={`/genres/${genre.id}`}>
                                        <span className="py-2 px-3 mr-2 text-lg font-semibold rounded-full bg-violet-500 text-white">{genre.title}</span>
                                    </Link>
                                ))}
                            </div>}
                    <p className="text-xl border-t-2 py-5  border-white">{movie.description}</p>
                    <div className="item-center justify-center m-auto my-10  transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-300">
                        <VioletLinkButton path="/booking" buttonName="Book Now" />
                    </div>
                </div>
            </section>
            <section className="flex flex-col m-auto max-w-screen-lg">
                <h2 className="p-5 text-5xl text-violet-900 mb-10">Reviews</h2>
                {loggedIn && <ReviewForm movieId={movie._id} />} 
                <div>
                {
                    reviews.map(review => {
                        return <ReviewCard key={review._id} reviews={review} />
                    })
                }
                </div>
            </section>
        </main>
        </>
    )
}

export default Movie

