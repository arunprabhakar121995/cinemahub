import React, { useRef } from "react";
import MovieCard from "../../Components/Cards/movieCard";
import axios from "axios";
import { useLoaderData, useNavigate } from "react-router-dom";
import LanguageCard from "../../Components/Cards/languageCard";
const DB_URL = import.meta.env.VITE_DB_URL

export async function loader({ params, request }) { // request is to get URL parameters
    // Taking URL parameters
    const url = new URL(request.url);
    const selectedLanguage = url.searchParams.get("languages")
    let moviesURL
    if(selectedLanguage){
        moviesURL = `${DB_URL}/movies?languages=${selectedLanguage}`
    }
    else{
        moviesURL = `${DB_URL}/movies`
    }

    // Use Promise.all to fetch both movies and languages concurrently
    const [moviesResponse, languageResponse] = await Promise.all([
        axios.get(moviesURL),
        axios.get(`${DB_URL}/languages`)
    ]);

    const movies = moviesResponse.data; // This line is because axios gives data inside data field
    const languages = languageResponse.data;

    return { movies, languages, selectedLanguage };
}

function Movies(props){
    const { movies, languages, selectedLanguage } = useLoaderData();
    const languageRef = useRef(null)

    const navigate = useNavigate() //To navigate for refresh

    function changeLanguage(){
        const selectedLanguage = languageRef.current.value
        const url = `/movies?languages=${selectedLanguage}`
        navigate(url)
        }
    return(
    <main>
        <section className="p-12">
            <div className="container m-auto">
            <div className="flex flex-col sm:flex-row m-auto items-center justify-between">
            <h2 className="text-5xl p-5 font-bold font-sanserif mb-5">{selectedLanguage? `${selectedLanguage} Movies` : "All Movies"}</h2>
                    <select className="h-10 border-2 border-red-950 rounded-xl pl-3 mb-5 text-red-950" ref={languageRef} onChange={changeLanguage}>
                        <option value="">All Languages</option>
                        {
                            languages.map(language => {
                                return(
                                    <LanguageCard key={languages._id} language={language} /> 
                                )
                            })
                        }
	                </select>
                </div>
                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-5">
                    {
                        movies.map(movie => {
                            return(
                                <MovieCard key={movies._id} movie={movie} />
                            )
                        })
                    }                        
                </div>
            </div>
        </section>
    </main>
    )
}

export default Movies