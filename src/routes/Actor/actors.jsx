import React from "react";
import { Link,useLoaderData } from "react-router-dom";
import ActorCard from "../../Components/Cards/actorCard";
import axios from "axios";
const DB_URL = import.meta.env.VITE_DB_URL

  export async function loader() {
    const response = await axios.get(`${DB_URL}/actors`);
    const actors = response.data  //This line is coz axios give data inside data field
    return { actors };
  }
function Actors(props){
    const { actors } = useLoaderData();
    return(
        <>
        <main className="my-10">
        <section className="p-5">
            <div className="container mx-auto">
                <h2 className="text-5xl py-5 px-10 font-bold font-sanserif mb-5">Actors</h2>
                <div className="grid sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 p-5">
                    {
                        actors.map(actor => {
                            return(
                                <ActorCard key={actors._id} actor={actor} />
                            )
                        })
                    }                        
                </div>
            </div>
        </section>
        </main>
        </>
    )
}

export default Actors