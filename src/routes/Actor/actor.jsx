import React from "react";
import { Link, useLoaderData } from "react-router-dom";
import axios from "axios";
const DB_URL = import.meta.env.VITE_DB_URL


  export async function loader({params}) {
    const response = await axios.get(`${DB_URL}/actors/${params.actorId}`);
    const actor = response.data  //This line is coz axios give data inside data field

        // Convert ISO string date to text format
        const bornDate = new Date(actor.born).toLocaleDateString('en-IN', {
            timeZone: 'Asia/Kolkata', // Indian time zone
            year: 'numeric',
            month: 'long',
            day: 'numeric',
        });

        actor.formattedBornDate = bornDate;
    return { actor };
  }
function Actor(props){
    const { actor } = useLoaderData();
    return(
        <>
        <main>
        <section className="container mx-auto flex flex-col lg:grid grid-cols-2 my-16 px-10 gap-20 justify-center items-center">
                <img className="flex items-center w-full px-10 rounded-full object-cover drop-shadow-xl" src={actor.image} alt="" />
                <div className="flex flex-col mx-auto justify-center bg-violet-200 p-5 rounded-xl">
                    <h1 className="text-4xl lg:text-5xl font-bold my-10">{actor.name} 
                        {actor.alsoknownas && <span className="text-4xl lg:text-5xl font-bold my-10">({actor.alsoknownas})</span> }</h1>                   
                    <span className="py-2 text-xl border-t-2  border-white">{`Occupation : ${actor.occupation}`}</span>
                    <span className="py-5 text-2xl border-t-2  border-white">{`Born: ${actor.formattedBornDate}`}</span> 
                    {actor.birthplace && <span className="py-2 text-xl border-t-2  border-white">{`Birthplace : ${actor.birthplace}`}</span> }
                    {actor.spouse && <span className="py-2 text-xl border-t-2  border-white">{`Spouse : ${actor.spouse}`}</span> }
                    {actor.children && <span className="py-2 text-xl border-t-2  border-white">{`Children : ${actor.children}`}</span>}
                    <p className="py-5 text-xl border-t-2  border-white">{actor.about}</p>
                </div>
            </section>
        </main>
        </>
    )
}

export default Actor