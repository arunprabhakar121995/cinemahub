import React, { useEffect,useState } from "react";
import { Link, Outlet } from "react-router-dom";
import DropdownButton from "../Components/Button/dropdownButton";
import { useDispatch, useSelector } from 'react-redux'
import { changeLoginStatus } from "../features/login/loginSlice";
import axios from "axios";
const DB_URL = import.meta.env.VITE_DB_URL


export default function Root() {
    const loggedIn = useSelector(state => state.login.loggedIn)
    const dispatch = useDispatch()

    useEffect(() => {
        axios.get(`${DB_URL}/auth/verify`, { withCredentials: true})
            .then(response => {
                dispatch(changeLoginStatus(true))
            })
            .catch(error => {
                dispatch(changeLoginStatus(false))
            });
    }, [dispatch]);
    return (
      <>
        <header className="bg-violet-600 text-white shadow-md shadow-violet-600 px-5">
            <div className="lg:container mx-auto flex justify-between items-center min-h-24">
            <h1 className="text-5xl font-bold">CinemaHub</h1>
            <DropdownButton />
            <nav className="hidden md:flex">
                <ul className="flex flex-row gap-5">
                    <li>
                        <Link className="text-lg hover:text-xl lg:text-xl lg:hover:text-2xl hover:font-bold hover:animate-pulse" to = "/">Home</Link>
                    </li>
                    <li>
                        <Link className="text-lg hover:text-xl lg:text-xl lg:hover:text-2xl hover:font-bold hover:animate-pulse" to = "/movies">Movies</Link>
                    </li>
                    <li>
                        <Link className="text-lg hover:text-xl lg:text-xl lg:hover:text-2xl hover:font-bold hover:animate-pulse" to = "/actors">Actors</Link>
                    </li>
                    <li>
                        <Link className="text-lg hover:text-xl lg:text-xl lg:hover:text-2xl hover:font-bold hover:animate-pulse" to = "/genres">Genres</Link>
                    </li>
                    
                    {
                        loggedIn?
                        (<li>
                            <Link className="text-xl hover:text-2xl lg:text-2xl lg:hover:text-3xl hover:font-bold hover:animate-pulse" to = "/logout">Logout</Link>
                        </li>) :
                        (<li>
                            <Link className="text-xl hover:text-2xl lg:text-2xl lg:hover:text-3xl hover:font-bold hover:animate-pulse" to = "/login">LogIn</Link>
                        </li>)
                    }
                    
                </ul>
            </nav>
            </div>
        </header>
        <Outlet />
        <footer>

        </footer>
      </>
    )
  }