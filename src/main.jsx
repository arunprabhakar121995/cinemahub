import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import './index.css'
import store from './app/store'  
import { Provider } from 'react-redux'
import Root from "./routes/root";
import ErrorPage from "./error-page";
import Home, { loader as HomeLoader } from './routes/home';
import Actors, { loader as ActorsLoader } from './routes/Actor/actors';
import Actor, { loader as ActorLoader } from './routes/Actor/actor';
import Movies, { loader as MoviesLoader} from './routes/Movie/movies';
import Movie, { loader as MovieLoader} from './routes/Movie/movie';
import Genre, { loader as GenreLoader } from './routes/Genre/genre';
import Genres, { loader as GenresLoader } from './routes/Genre/genres';
import SignUp from './routes/signup';
import Login from './routes/login';
import Logout from './routes/logout';
import Booking from './routes/Booking/booking';

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <Home />,
        loader: HomeLoader
      },
      
      {
        path: "/signup",
        element: <SignUp />,
      },
      {
        path: "/login",
        element: <Login />,
      },
      {
        path: "/logout",
        element: <Logout />,
      },
      {
        path: "/actors",
        element: <Actors />,
        loader: ActorsLoader
      },
      {
        path: "/actors/:actorId",
        element: <Actor />,
        loader: ActorLoader
      },
      {
        path: "/movies",
        element: <Movies />,
        loader: MoviesLoader,
      },
      {
        path: "/movies/:movieId",
        element: <Movie />,
        loader: MovieLoader,
      },
      {
        path: "/genres/:genreId",
        element: <Genre />,
        loader: GenreLoader
      },
      {
        path: "/genres",
        element: <Genres />,
        loader: GenresLoader,
      },
      {
        path: "/booking",
        element:<Booking />
      }
    ]
  }
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
)
